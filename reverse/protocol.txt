# Here is an example of a raw read command:
[
	# Response := [Raw Data Block] <ESC> [Status Byte]
	# [Raw Data Block] := <ESC> s  [Raw Data] ?  <FS> <ESC> [Status]
	#                     1B    73 [Raw Data] 3F 1C   1B    [Status]
	#
	# [Raw Data] := <ESC> 1 [L1] [string1] <ESC> 2 [L2] [string2] <ESC> 3 [L3] [string3]
        #               1B    ? Len  TheString 1B    ? Len  TheString 1B    ? Len  TheString
	#
	[
		191, 27, 115, 27, 1, 55, 162, 141, 160,
		64, 141, 10, 204, 138, 208, 169, 94, 161,
		26, 21, 105, 80, 38, 65, 175, 130, 117,
		105, 214, 160, 68, 173, 18, 149, 200, 64,
		36, 90, 129, 2, 16, 2, 4, 8, 16,
		32, 64, 129, 124, 86, 100, 84, 177, 98,
		132, 45, 157, 151, 198, 128, 0, 27, 2,
		25
	],
	[
 		96, 211, 66, 22, 146, 112, 105, 9, 192,
		180, 141, 168, 114, 43, 18, 112, 68, 32,
		16, 215, 33, 202, 51, 63, 216, 0, 27,
		3, 0, 63, 28, 27, 48, 149, 200, 64,
		36, 90, 129, 2, 16, 2, 4, 8, 16,
		32, 64, 129, 124, 86, 100, 84, 177, 98,
		132, 45, 157, 151, 198, 128, 0, 27, 2,
		25
	],
	None
]

# Here is the decoded data as ASCII:
[
	#  [RawDataBlock]<ESC>[StatusByte]
	[
		# stuff
		<UNKNOWN:191>, <ESC>, s, <ESC>, 0x01, 55 (len), 162, 141, 160,
		64, 141, 10, 204, 138, 208, 169, 94, 161,
		26, 21, 105, 80, 38, 65, 175, 130, 117,
		105, 214, 160, 68, 173, 18, 149, 200, 64,
		36, 90, 129, 2, 16, 2, 4, 8, 16,
		32, 64, 129, 124, 86, 100, 84, 177, 98,
		132, 45, 157, 151, 198, 128, <NULL>, <ESC>, 0x2,
		25 (len),

	],
	[
 		96, 211, 66, 22, 146, 112, 105, 9, 192,
		180, 141, 168, 114, 43, 18, 112, 68, 32,
		16, 215, 33, 202, 51, 63, 216, <NULL>, <ESC>,
		0x3, <NULL>, 63, 28, 27, 48, 149, 200, 64,
		36, 90, 129, 2, 16, 2, 4, 8, 16,
		32, 64, 129, 124, 86, 100, 84, 177, 98,
		132, 45, 157, 151, 198, 128, 0, <ESC>, 0x2,
		<EM>
	],
	# Blank track
]
